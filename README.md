# volunteer-list

.Net Core & Sql Server to be used to develop this project
 

**Problem Statement**

An application to handle the CRUD operation for the Volunteers who can help selflessly in case of emergencies.

**Audiences/Actors involved:**
- Public Users – Can view only without authentication.
- Volunteers – Can add and edit themselves as respective profiles being the volunteers.

**Flow/Use case involved:**
- A dashboard to display the list of active volunteers and the regions they are active in & other details of the volunteers (explained in Annexure 1)
- Users can add themselves as volunteers on clinking a global button Be a Volunteer and fill in their details mentioned in Annexure 1.
- A section in the dashboard where the list of alerts will be listed. The alerts can be added by any public user. The details during alert (explained in Annexure 2).
- No Authentication or Authorization needed.

**Technologies to be used:**
- .Net Core using WEB API only
- SQL Server
- Entity Framework using LINQ
- Gitlab
- Angular JS - For front end completely

<details>
<summary>Annexure 1</summary>

- Name of Volunteer (Mandatory)

- Contact Number (Mandatory)

- Regions Covered (Regions enetered comma separated) (Mandatory)

- Twitter handle (Optional)

- Facebook URL (Optional)
</details>

<details>
<summary>Annexure 2</summary>

- Title of Alert (Mandatory)

- Description of Alert (Mandatory)

- Contact Number Who created Alert (Mandatory)

- Address of Alert (Mandatory)

- Tweet Reference (Optional)

- Facebook Reference (Optional)

- Donations URL (Optional)

- Created By User Name (Mandatory)
</details>


